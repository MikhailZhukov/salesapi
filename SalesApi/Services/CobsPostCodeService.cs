﻿using System;
using System.ServiceModel;
using AutoMapper;
using CobsPostcodeComponent;
using Microsoft.Extensions.Configuration;
using SalesApi.Helpers;
using SalesApi.Utils;

namespace SalesApi.Services
{
    internal class CobsPostCodeService : ICobsPostCodeService
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _appConfig;

        public CobsPostCodeService(IMapper mapper, IConfiguration appConfig)
        {
            _mapper = mapper;
            _appConfig = appConfig;
        }

        public ResellerApiBellaService.PostCodeCheckResultDto CheckPostCode(PostCodeCheckRequest request, string methodName)
        {
            return LogExecutionTime.Execute(
                () =>
            {
                var client = CreateClient();

                try
                {
                    var additionalInfo = new PostCodeAdditionalInfo()
                    {
                        componentName = "ResellerApi",
                        processName = methodName
                    };

                    var result = client.PostCodeCheckAsync(request, additionalInfo).ConfigureAwait(false).GetAwaiter().GetResult();

                    return _mapper.Map<ResellerApiBellaService.PostCodeCheckResultDto>(result);
                }
                catch (Exception ex)
                {
                    client.Abort();
                    throw ex;
                }
            }, nameof(CheckPostCode));
        }

        private CobsPostcodeServiceClient CreateClient()
        {
            var address = _appConfig.GetCobsPostcodeServiceUrl();
            return new CobsPostcodeServiceClient()
            {
                Endpoint =
                {
                    Address = new EndpointAddress(address)
                }
            };
        }
    }
}
