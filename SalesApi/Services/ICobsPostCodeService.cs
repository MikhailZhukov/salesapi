﻿using ResellerApiBellaService;

namespace SalesApi.Services
{
    public interface ICobsPostCodeService
    {
        PostCodeCheckResultDto CheckPostCode(CobsPostcodeComponent.PostCodeCheckRequest request, string methodName);
    }
}
