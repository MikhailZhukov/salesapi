﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ResellerApiBellaService;
using SalesApi.Helpers;
using SalesApi.Models.V2;
using SalesApi.Services;
using SalesApi.Utils;

namespace SalesApi.Controllers.V2
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiConventionType(typeof(ApiConventions))]
    [ApiController]
    public class ExampleV2Controller : ControllerBase
    {
        private readonly ILogger<ExampleV2Controller> _logger;
        private readonly ResellerApiServiceClient _resellerApiService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _appConfig;
        private readonly ICobsPostCodeService _cobsPostcodeService;
        private readonly string _voipTypeOfServiceId;

        public ExampleV2Controller(ILogger<ExampleV2Controller> logger, ResellerApiServiceClient resellerApiService, IMapper mapper, IConfiguration appConfig, ICobsPostCodeService postcodeService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _resellerApiService = resellerApiService ?? throw new ArgumentNullException(nameof(resellerApiService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _appConfig = appConfig ?? throw new ArgumentNullException(nameof(appConfig));
            _cobsPostcodeService = postcodeService;
            _voipTypeOfServiceId = appConfig.GetVoipTypeOfServiceId();
        }

        [HttpGet]
        [Route("version")]
        public string GetControllerVersion()
        {
            return "v2";
        }

        [HttpGet]
        [Route("example")]
        public ExampleModelForV2 GetExampleModelForV2()
        {
            return new ExampleModelForV2();
        }
    }
}
