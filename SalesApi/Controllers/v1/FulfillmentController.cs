﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ResellerApiBellaService;
using SalesApi.Helpers;
using SalesApi.Services;
using SalesApi.Utils;

namespace SalesApi.Controllers.V1
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiConventionType(typeof(ApiConventions))]
    [ApiController]
    public class FulfillmentController : ControllerBase
    {
        private readonly ILogger<FulfillmentController> _logger;
        private readonly ResellerApiServiceClient _resellerApiService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _appConfig;
        private readonly ICobsPostCodeService _cobsPostcodeService;
        private readonly string _voipTypeOfServiceId;

        public FulfillmentController(ILogger<FulfillmentController> logger, ResellerApiServiceClient resellerApiService, IMapper mapper, IConfiguration appConfig, ICobsPostCodeService postcodeService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _resellerApiService = resellerApiService ?? throw new ArgumentNullException(nameof(resellerApiService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _appConfig = appConfig ?? throw new ArgumentNullException(nameof(appConfig));
            _cobsPostcodeService = postcodeService;
            _voipTypeOfServiceId = appConfig.GetVoipTypeOfServiceId();
        }

        /// <summary>
        /// Get available proposition by id
        /// </summary>
        /// <param name="propositionId"> Id of proposition </param>
        /// <param name="zip"> Postcode </param>
        /// <param name="houseNumber"> House number </param>
        /// <param name="extension" required="false"> Extension </param>
        /// <returns> Available proposition </returns>
        [HttpGet("Proposition/{propositionId}/{zip}/{houseNumber}")]
        public async Task<Models.V1.AvailableProposition> GetProposition(string propositionId, string zip, int houseNumber, string extension = null)
        {
            var bellaProposition = await GetAvailablePropositionById(propositionId, zip, houseNumber.ToString(), extension);
            CrmDiscountDto[] bellaPropositionDiscounts = await _resellerApiService.ResellerApiGetDiscountsByPropositionIdAsync(propositionId);

            if (bellaProposition == null)
            {
                throw new FaultException<DomainFault>(new DomainFault() { Code = DomainError.PropositionNotFound, Message = "Proposition not found!" }, null, null, null);
            }

            var proposition = _mapper.Map<Models.V1.AvailableProposition>(bellaProposition);

            if (bellaPropositionDiscounts.Any())
            {
                foreach (var discount in bellaPropositionDiscounts)
                {
                    var bundles = proposition.Products.SelectMany(s => s.Bundles).Where(s => s.Id == discount.bundleId);

                    foreach (var b in bundles)
                    {
                        b.PropositionDiscount = _mapper.Map<Models.V1.PropositionDiscount>(discount);
                    }
                }
            }

            return proposition;
        }

        /// <summary>
        /// Creates Triple Play order in ET
        /// </summary>
        /// <param name="createTriplePlayOrder"> Order creation object </param>
        /// <returns> Id of the created order </returns>
        [HttpPost]
        [Route("CreateOrder")]
        public async Task<string> CreateOrder([FromBody] Models.V1.CreateTriplePlayOrder createTriplePlayOrder)
        {
            string propositionId = createTriplePlayOrder.PropositionId;

            _logger.LogDebug("Proposition Id from request: " + propositionId);
            _logger.LogDebug("Trying to map request object...");
            CrmPortalCreateTriplePlayOrderRequest innerRequest = _mapper.Map<CrmPortalCreateTriplePlayOrderRequest>(createTriplePlayOrder);
            _logger.LogDebug("Success");

            var proposition = await GetAvailablePropositionById(propositionId, createTriplePlayOrder.ShippingAddress.Zip, createTriplePlayOrder.ShippingAddress.HouseNumber, createTriplePlayOrder.ShippingAddress.Extension);

            if (proposition == null)
            {
                _logger.LogInformation("Proposition specified in the request was not found or not available for address in the request");
                throw new FaultException<DomainFault>(new DomainFault() { Code = DomainError.PropositionNotFound, Message = "Proposition with id " + propositionId + "is not available for current address" }, null, null, null);
            }

            AvailableProduct voipProduct = null;
            innerRequest.voipGiven = false;

            if (proposition.products != null && proposition.products.Any())
            {
                voipProduct = proposition.products.First(x => x.typeOfServiceId == _voipTypeOfServiceId);
            }
            else
            {
                _logger.LogInformation("List of products in the proposition is empty. Search of the voip product was not performed");
            }

            if (voipProduct != null)
            {
                _logger.LogInformation("Voip product was found. Searching for Phone Basic bundle");
                AvailableBundle voipBasic = null;

                if (voipProduct.bundles != null && voipProduct.bundles.Any())
                {
                    voipBasic = voipProduct.bundles.First(x => x.isMain);
                }

                if (voipBasic != null && createTriplePlayOrder.SubscribeBundleWithDiscounts != null && createTriplePlayOrder.SubscribeBundleWithDiscounts.FindIndex(i => i.BundleId == voipBasic.id) != -1)
                {
                    innerRequest.voipGiven = true;
                }
            }

            if (innerRequest.voipGiven)
            {
                if (!string.IsNullOrEmpty(innerRequest.portingNumber))
                {
                    innerRequest.portingNumberGiven = true;
                }
                else
                {
                    innerRequest.portingNumberGiven = false;
                    innerRequest.portingNumber = null;
                }
            }
            else
            {
                innerRequest.voipSettings = null;
            }

            innerRequest.startOn = innerRequest.startWishDate;
            innerRequest.endOn = innerRequest.startWishDate.AddMonths(proposition.contractDuration);

            // default values
            innerRequest.isOrderFromMyNle = false;
            innerRequest.customer.type = CustomerType.Regular;
            innerRequest.serviceCategoryId = "TriplePlay";
            innerRequest.type = OrderType.New;

            return await _resellerApiService.ResellerApiCreateOrderAsync(innerRequest);
        }

        [HttpGet]
        [Route("GetPossibleWishdate/{isOverstapservice}/{isUrgent}/{isZiggoOrKpn?}")]
        public async Task<DateTime> GetPossibleWishdate(bool isOverstapservice, bool isUrgent, bool? isZiggoOrKpn = null)
        {
            return await _resellerApiService.ResellerApiGetPossibleWishdateAsync(isOverstapservice, isUrgent, isZiggoOrKpn ?? false);
        }

        [HttpGet]
        [Route("version")]
        public string GetControllerVersion()
        {
            return "v1";
        }

        private async Task<AvailableProposition> GetAvailablePropositionById(string propositionId, string zip, string houseNumber, string extension)
        {
            var postcodeCheckRequest = new CobsPostcodeComponent.PostCodeCheckRequest()
            {
                houseNumber = houseNumber,
                postalCode = zip,
                isNeedToBeStore = false,
                houseNumberAddition = extension ?? string.Empty,
            };

            var postcodeResultDto = _cobsPostcodeService.CheckPostCode(postcodeCheckRequest, "GetAvailablePropositionById");
            var bellaResponse = await _resellerApiService.ResellerApiGetAvailablePropositionsByZipCodeWithPostCodeCheckResultDtoAsync(postcodeResultDto);
            var proposition = bellaResponse.propositions.FirstOrDefault(p => p.id == propositionId);

            return proposition;
        }
    }
}
