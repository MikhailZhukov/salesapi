﻿using SalesApi.Models.V1;

namespace SalesApi.Models.V1
{
    public class TvChannelAction
    {
        public TvChannelActionType Action { get; set; }

        public string BundleId { get; set; }

        public string DiscountId { get; set; }

        public string PropositionId { get; set; }
    }
}
