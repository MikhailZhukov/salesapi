﻿using System;
using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class CreateOrder
    {
        public string VendorId { get; set; }

        public string PropositionId { get; set; }

        public List<SubscribeBundleWithDiscount> SubscribeBundleWithDiscounts { get; set; }

        public List<string> CashbackIdList { get; set; }

        public bool MandateGiven { get; set; }

        public CreateMandate Mandate { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public CreateOrderCustomerInfo Customer { get; set; }

        public Address ShippingAddress { get; set; }

        public DateTime SaleDate { get; set; }

        public DateTime StartWishDate { get; set; }
    }
}
