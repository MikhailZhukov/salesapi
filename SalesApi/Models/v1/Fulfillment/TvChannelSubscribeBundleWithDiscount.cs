﻿namespace SalesApi.Models.V1
{
    public class TvChannelSubscibeBundleWithDiscount : SubscribeBundleWithDiscount
    {
        public string PropositionId { get; set; }
    }
}
