﻿namespace SalesApi.Models.V1
{
    public class CreateTriplePlayOrder : CreateOrder
    {
        public bool IsInstallationHelpNeeded { get; set; }

        public bool IsRemoteInstallationHelpNeeded { get; set; }

        public string PortingNumber { get; set; }

        public string CurrentProvider { get; set; }

        public bool IsSwitchServiceNeeded { get; set; }

        public CreateVoipSettings VoipSettings { get; set; }
    }
}
