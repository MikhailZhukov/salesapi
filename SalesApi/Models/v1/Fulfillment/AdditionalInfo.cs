﻿namespace SalesApi.Models.V1
{
    public class AdditionalInfo
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
