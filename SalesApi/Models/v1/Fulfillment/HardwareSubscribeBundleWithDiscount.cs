﻿using System.ComponentModel.DataAnnotations;

namespace SalesApi.Models.V1
{
    public class HardwareSubscribeBundleWithDiscount : SubscribeBundleWithDiscount
    {
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer number")]
        public int QuantityToAdd { get; set; }
    }
}
