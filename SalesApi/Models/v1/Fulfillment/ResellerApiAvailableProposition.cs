﻿namespace SalesApi.Models.V1
{
    public class ResellerApiAvailableProposition
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int Duration { get; set; }
    }
}
