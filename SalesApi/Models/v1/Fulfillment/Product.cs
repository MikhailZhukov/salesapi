﻿using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class Product
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string TypeOfServiceId { get; set; }

        public List<Bundle> Bundles { get; set; }
    }
}
