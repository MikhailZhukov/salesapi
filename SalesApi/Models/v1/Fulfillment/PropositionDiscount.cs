﻿using System;

namespace SalesApi.Models.V1
{
    public class PropositionDiscount
    {
        public double AbsoluteAmount { get; set; }

        public int BillPeriodQuantity { get; set; }

        public string BundleId { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

       public double PercentageAmount { get; set; }

        public DateTime ToDateTime { get; set; }

        public DiscountType Type { get; set; }
    }
}
