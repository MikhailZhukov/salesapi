﻿namespace SalesApi.Models.V1
{
    public class CreateAdjustingOrder
    {
        //public AdjustingOrderType Type { get; set; } // Kharkov team will use specific API method that defines the type of order. It should be assigned within
        public string ServiceAccountId { get; set; }
        // public string CreatedBy { get; set; } // Creator is always customer. There is no need for Kharkov to enter it manually in the request
    }
}
