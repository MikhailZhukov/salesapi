﻿namespace SalesApi.Models.V1
{
    public class Address
    {
        public string Zip { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string HouseNumber { get; set; }

        public string Extension { get; set; }
    }
}
