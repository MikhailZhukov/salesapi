﻿using System;

namespace SalesApi.Models.V1
{
    public class CreateOrderCustomerInfo
    {
        public Address BillingAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Initials { get; set; }

        public string Prefix { get; set; }

        public Gender Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public ContactInfo ContactInfo { get; set; }
    }
}
