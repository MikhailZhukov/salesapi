﻿namespace SalesApi.Models.V1
{
    public class HardwareOrder
    {
        public string BundleId { get; set; }

        public string DiscountId { get; set; }

        public int QuantityToAdd { get; set; }
    }
}
