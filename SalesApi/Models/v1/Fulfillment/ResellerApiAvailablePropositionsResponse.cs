﻿using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class ResellerApiAvailablePropositionsResponse
    {
        public List<ResellerApiAvailableProposition> Propositions { get; set; }

        public List<string> PossibleExtensions { get; set; }
    }
}
