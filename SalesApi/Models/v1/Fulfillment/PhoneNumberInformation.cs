﻿using SalesApi.Models.V1;

namespace SalesApi.Models.V1
{
    public class PhoneNumberInformation
    {
        public string PhoneNumber { get; set; }

        public PhoneType PhoneType { get; set; }
    }
}
