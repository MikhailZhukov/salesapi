﻿namespace SalesApi.Models.V1
{
    public class CreateUpgradeDslOrder : CreateAdjustingOrder
    {
        public HardwareSubscribeBundleWithDiscount[] HardwareOrders { get; set; }

        public SubscribeBundle SubscribeBundle { get; set; }
    }
}
