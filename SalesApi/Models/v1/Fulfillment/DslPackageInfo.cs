﻿using SalesApi.Models.V1;

namespace SalesApi.Models.V1
{
    public class DslPackageInfo
    {
        public string BundleId { get; set; }

        public string BundleName { get; set; }

        public double DiscountedPrice { get; set; }

        public int InternetSpeed { get; set; }

        public bool IsEmpty { get; set; }

        public bool IsOrderedInfoAvailable { get; set; }

        public LineType LineType { get; set; }

        public HardwareInfo ModemInfo { get; set; }

        public double Price { get; set; }
    }
}
