﻿using SalesApi.Models.V1;

namespace SalesApi.Models.V1
{
    public class CreateVoipSettings
    {
        public bool VisibleInPhoneBook { get; set; }

        public bool VisibleInElectronicPhoneBook { get; set; }

        public bool KnownInformationService { get; set; }

        public bool HidePhoneNumberOnInvoices { get; set; }

        public bool ShowPhoneNumberOnCalling { get; set; }

        public bool AllowMobileCall { get; set; }

        public bool AllowInternationalCall { get; set; }

        public ServiceNumbers BlockServiceNumbers { get; set; }
    }
}
