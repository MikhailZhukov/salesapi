﻿using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class ContactInfo
    {
        public string Email { get; set; }

        public List<PhoneNumberInformation> PhoneNumberInformation { get; set; }
    }
}
