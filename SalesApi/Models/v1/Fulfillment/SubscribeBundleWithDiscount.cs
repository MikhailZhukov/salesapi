﻿namespace SalesApi.Models.V1
{
    public class SubscribeBundleWithDiscount
    {
        public string BundleId { get; set; }

        public string DiscountId { get; set; }
    }
}
