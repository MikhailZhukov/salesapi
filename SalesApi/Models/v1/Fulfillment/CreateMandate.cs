﻿using System;

namespace SalesApi.Models.V1
{
    public class CreateMandate
    {
        public string MemberIban { get; set; }

        public DateTime StartOn { get; set; }

        public string City { get; set; }
    }
}
