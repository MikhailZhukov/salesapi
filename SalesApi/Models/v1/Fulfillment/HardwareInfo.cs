﻿namespace SalesApi.Models.V1
{
    public class HardwareInfo
    {
        public string BundleId { get; set; }

        public string BundleName { get; set; }

        public int Quantity { get; set; }
    }
}
