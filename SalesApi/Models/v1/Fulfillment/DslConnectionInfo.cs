﻿namespace SalesApi.Models.V1
{
    public class DslConnectionInfo
    {
        public PackageInfo AdditionalStbPackageInfo { get; set; }

        public DslUpgradePackageInfo[] BundlesForUpgrade { get; set; }

        public bool CanOrderExtraStb { get; set; }

        public int CurrentStbCount { get; set; }

        public bool IsUpgradePossible { get; set; }

        public int MaxStbCount { get; set; }

        public int PendingStbCount { get; set; }

        public DslPackageInfo ProcessingPackage { get; set; }

        public DslPackageInfo SubscribedPackage { get; set; }
    }
}
