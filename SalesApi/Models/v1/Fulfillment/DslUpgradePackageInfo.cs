﻿using SalesApi.Models.V1;

namespace SalesApi.Models.V1
{
    public class DslUpgradePackageInfo
    {
        public string BundleId { get; set; }

        public string BundleName { get; set; }

        public string DiscountId { get; set; }

        public double DiscountedPrice { get; set; }

        public int InternetSpeed { get; set; }

        public LineType LineType { get; set; }

        public int MaxStbCount { get; set; }

        public double Price { get; set; }
    }
}
