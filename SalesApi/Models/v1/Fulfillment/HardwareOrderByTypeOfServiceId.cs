﻿namespace SalesApi.Models.V1
{
    public class HardwareOrderByTypeOfServiceId
    {
        public HardwareOrder[] Orders { get; set; }

        public string TypeOfServiceId { get; set; }
    }
}
