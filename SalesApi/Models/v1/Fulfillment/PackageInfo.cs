﻿using System;

namespace SalesApi.Models.V1
{
    public class PackageInfo
    {
        public string BundleId { get; set; }

        public string BundleName { get; set; }

        public DateTime ChangeDate { get; set; }

        public string DiscountId { get; set; }

        public string DiscountName { get; set; }

        public double DiscountedPrice { get; set; }

        public bool IsMain { get; set; }

        public bool IsMonthlyTerminable { get; set; }

        public double Price { get; set; }

        public string PropositionId { get; set; }

        public PackageStatus Status { get; set; }
    }
}
