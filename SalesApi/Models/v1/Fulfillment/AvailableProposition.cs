﻿using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class AvailableProposition
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public List<Product> Products { get; set; }

        public bool IsDefault { get; set; }
    }
}
