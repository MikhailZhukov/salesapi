﻿using System.Collections.Generic;

namespace SalesApi.Models.V1
{
    public class Bundle
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public CobsProductType ProductType { get; set; }

        public string ExternalCode { get; set; }

        public decimal Price { get; set; }

        public LineType LineType { get; set; }

        public int InternetSpeed { get; set; }

        public HardwareType HardwareType { get; set; }

        public string NetworkType { get; set; }

        public string Technology { get; set; }

        public BundlePeriodicity BundlePeriodicity { get; set; }

        public bool IsMain { get; set; }

        public List<AdditionalInfo> AdditionalInfos { get; set; }

        public PropositionDiscount PropositionDiscount { get; set; }

        public string GiftType { get; set; }
    }
}
