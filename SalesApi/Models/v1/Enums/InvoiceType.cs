﻿namespace SalesApi.Models.V1
{
    public enum InvoiceType
    {
        First,
        Recurrent,
        Dunning,
        EndNote,
        PartOfPaymentArrangement
    }
}
