﻿namespace SalesApi.Models.V1
{
    public enum InvoiceStatus
    {
        Issued,
        Open,
        Paid,
        Canceled,
        Dunning,
        AwaitsMerge,
        Merged,
        Uncollectable,
        PaymentArrangement,
    }
}
