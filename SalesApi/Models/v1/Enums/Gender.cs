﻿namespace SalesApi.Models.V1
{
    public enum Gender
    {
        Unknown,
        Male,
        Female
    }
}
