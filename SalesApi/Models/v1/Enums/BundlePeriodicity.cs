﻿namespace SalesApi.Models.V1
{
    public enum BundlePeriodicity
    {
        OneTime,
        Monthly,
        Quarterly,
        Deposit
    }
}
