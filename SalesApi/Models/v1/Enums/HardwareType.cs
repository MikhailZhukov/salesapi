﻿namespace SalesApi.Models.V1
{
    public enum HardwareType
    {
        NotHardware,
        MainStb,
        AdditionalStb,
        Modem,
        InternetExtender,
    }
}
