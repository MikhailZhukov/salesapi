﻿namespace SalesApi.Models.V1
{
    public enum AdjustingOrderType
    {
        Base,
        StartTv,
        StopTv,
        StartVoip,
        StopVoip,
        UpgradeDsl,
        OrderHardware,
        PackageAdd,
        PackageDrop,
        AutoProlongation,
        CeaseDsl,
        MoveDsl,
        ChangePriceForAutoProlongation
    }
}
