﻿namespace SalesApi.Models.V1
{
    public enum SubscriptionInfoState
    {
        NotOrdered,
        EnableInProgress,
        Active,
        ScheduledToStop,
        CannotBeOrdered
    }
}