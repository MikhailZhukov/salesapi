﻿namespace SalesApi.Models.V1
{
    public enum CustomerContactType
    {
        Base = 0,
        IncomingEmail = 1,
        OutgoingEmail = 2,
        IncomingCall = 3,
        OutgoingCall = 4,
        Remark = 5,
        Contract = 6,
        ProlongationContract = 7,
    }
}
