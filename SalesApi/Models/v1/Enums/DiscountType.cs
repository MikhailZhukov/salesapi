﻿namespace SalesApi.Models.V1
{
    public enum DiscountType
    {
        Absolute,
        Percentage
    }
}
