﻿namespace SalesApi.Models.V1
{
    public enum DayPart
    {
        Morning,
        Afternoon
    }
}
