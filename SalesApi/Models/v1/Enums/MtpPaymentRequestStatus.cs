﻿namespace SalesApi.Models.V1
{
    public enum MtpPaymentRequestStatus
    {
        Unknown = 0,

        /// <summary>
        /// No reaction to the message
        /// </summary>
        NoReaction = 101,

        /// <summary>
        /// Message was read
        /// </summary>
        MessageRead = 300,

        /// <summary>
        /// Customer clicked on the link
        /// </summary>
        VisitedWebsite = 500,

        /// <summary>
        /// Payment started, not finished
        /// </summary>
        PaymentStarted = 700,

        /// <summary>
        /// Payment is canceled by the customer
        /// </summary>
        PaymentAborted = 701,

        /// <summary>
        /// Payment failed. For example, balance deficit
        /// </summary>
        PaymentFailed = 702,

        /// <summary>
        /// Payment starts, status unknown
        /// </summary>
        PaymentInvalid = 703,

        /// <summary>
        /// Payments started but not completed on time
        /// </summary>
        PaymentExpired = 704,

        /// <summary>
        /// Full amount is paid
        /// </summary>
        Paid = 900,

        /// <summary>
        /// Debit failed
        /// </summary>
        Reversed = 998,

        /// <summary>
        /// Message could not be delivered
        /// </summary>
        Bounce = 999,
    }
}
