﻿namespace SalesApi.Models.V1
{
    public enum TvChannelActionType
    {
        Add,
        Drop
    }
}
