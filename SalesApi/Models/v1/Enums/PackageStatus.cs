﻿namespace SalesApi.Models.V1
{
    public enum PackageStatus
    {
        On,
        Off,
        Processing,
        Scheduled,
    }
}
