﻿namespace SalesApi.Models.V1
{
    public enum PaymentMethod
    {
        DirectDebit,
        MailToPay
    }
}
