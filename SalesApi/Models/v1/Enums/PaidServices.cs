﻿namespace SalesApi.Models.V1
{
    public enum PaidServices
    {
        BLOCK_ALL,
        BLOCK_ADULT_CHAT_AND_AMUSEMENT,
        BLOCK_ADULT_CHAT,
        ALLOW_ALL,
    }
}