﻿namespace SalesApi.Models.V1
{
    public enum CobsProductType
    {
        Internet,
        Voip,
        Tv,
        TvChannel,
        Hardware,
        NotCobsProduct
    }
}
