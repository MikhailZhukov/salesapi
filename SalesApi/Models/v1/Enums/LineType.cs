﻿namespace SalesApi.Models.V1
{
    public enum LineType
    {
        Copper,
        Fiber,
        NotLine
    }
}
