﻿namespace SalesApi.Models.V1
{
    public enum ServiceAccountStatus
    {
        NotActivated,
        Active,
        Closed,
        Canceled,
    }
}
