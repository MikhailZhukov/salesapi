﻿using System.Collections.Generic;

namespace SalesApi.Models.Common
{
    public class ErrorWrapper
    {
        private readonly List<Error> _errors;

        public ErrorWrapper(List<Error> errors) => _errors = errors;
    }
}
