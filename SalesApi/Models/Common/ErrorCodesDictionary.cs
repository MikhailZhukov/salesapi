﻿using System.Collections.Generic;

namespace SalesApi.Models.Common
{
    public static class ErrorCodesDictionary
    {
        public static Dictionary<int, string> ErrorCodes => new Dictionary<int, string>()
        {
           { 404, "Page not found" },
           { 400, "Bad request" },
           { 403, "Forbidden" },
           { 500, "Internal Service Error" },
           { 503, "Service Unavailable" }
        };
    }
}
