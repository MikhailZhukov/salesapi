﻿namespace SalesApi.Models.Common
{
    public class Error
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}
