﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ResellerApiBellaService;
using SalesApi.Models.Common;

namespace SalesApi.Middleware
{
    internal class ResponseWrapperMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ResponseWrapperMiddleware> _logger;

        public ResponseWrapperMiddleware(RequestDelegate next, ILogger<ResponseWrapperMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Middleware which catch errors from controller
        /// And set error codes to response with message
        ///     First of all catch DomainFaultError exception:
        ///         connected to MyNleService
        ///     Then catch communication exceptions
        ///         if service is down or not available
        ///     Finally catch generic exceptions
        ///         some generic exceptions
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>Task</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            var currentBody = context.Response.Body; // body state before calling any of the methods
            var memoryStream = new MemoryStream();
            context.Response.Body = memoryStream; //set the current response to the memorystream.

            try
            {
                await _next(context); // in case of an error, response body must be set to body state before calling any of the methods

                if (context.Response.StatusCode != StatusCodes.Status200OK)
                {
                    context.Response.Body = currentBody;

                    if (ErrorCodesDictionary.ErrorCodes.ContainsKey(context.Response.StatusCode))
                    {
                        await CreateError(context, context.Response.StatusCode, ErrorCodesDictionary.ErrorCodes[context.Response.StatusCode]);
                        return;
                    }
                    else
                    {
                        await CreateError(context, 500, ErrorCodesDictionary.ErrorCodes[500]);
                    }
                }

                context.Response.Body = currentBody;                //reset the body
                memoryStream.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(memoryStream))
                {
                    var readToEnd = reader.ReadToEnd(); // read successful response
                    var objResult = JsonConvert.DeserializeObject(readToEnd);
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new { data = objResult }, Formatting.Indented));
                }
            }
            catch (FaultException<DomainFault> ex)
            {
                context.Response.Body = currentBody;
                await HandleDomainFaultErrorAsync(context, ex);
            }
            catch (CommunicationException ex)
            {
                context.Response.Body = currentBody;
                await HandleCommunicationErrorAsync(context, ex);
            }
            catch (Exception ex)
            {
                context.Response.Body = currentBody;
                await HandleGenericErrorAsync(context, ex);
            }
            finally
            {
                memoryStream.Close();
            }
        }

        private async Task HandleGenericErrorAsync(HttpContext context, Exception exception)
        {
            _logger.LogError(exception.Message);

            await CreateError(context, 500, ErrorCodesDictionary.ErrorCodes[500]);
        }

        private async Task HandleCommunicationErrorAsync(HttpContext context, CommunicationException exception)
        {
            _logger.LogError(exception.Message);

            await CreateError(context, 503, ErrorCodesDictionary.ErrorCodes[503]);
        }

        private async Task HandleDomainFaultErrorAsync(HttpContext context, FaultException<DomainFault> exception)
        {
            _logger.LogError($"{exception} Detail: {exception.Detail.Message} Code: {exception.Detail.Code}");

            if (exception.Detail.Code == DomainError.DomainErrorMember)
            {
                await CreateError(context, 503, ErrorCodesDictionary.ErrorCodes[503]);
            }
            else
            {
                await CreateError(context, 400, exception.Detail.Message);
            }
        }

        [Produces("application/json")]
        private async Task CreateError(HttpContext context, int errorStatusCode, string errorMessage)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = errorStatusCode;
            List<Error> errors = new List<Error>()
            {
                new Error() { StatusCode = errorStatusCode, Message = errorMessage }
            };
            ErrorWrapper errorWrapper = new ErrorWrapper(errors);
            string s = JsonConvert.SerializeObject(errorWrapper, typeof(ErrorWrapper), null);
            await context.Response.WriteAsync(s);
        }
    }
}
