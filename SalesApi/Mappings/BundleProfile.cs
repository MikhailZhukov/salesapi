﻿using AutoMapper;
using V1 = SalesApi.Models.V1;

namespace SalesApi.Mappings
{
    internal class BundleProfile : Profile
    {
        public BundleProfile()
        {
            MapBundles();
        }

        private void MapBundles()
        {
            CreateMap<V1.SubscribeBundleWithDiscount,
                    ResellerApiBellaService.SubscribeBundleWithDiscountRequest>()
                .ForMember(x => x.propositionId, option => option.Ignore())
                .ForMember(x => x.startOrderId, option => option.Ignore())
                .ForMember(x => x.subscribedBundleId, option => option.Ignore());

            CreateMap<V1.SubscribeBundle, ResellerApiBellaService.SubscribeBundleWithDiscountRequest>()
                .ForMember(x => x.discountId, options => options.Ignore())
                .ForMember(x => x.propositionId, option => option.Ignore())
                .ForMember(x => x.startOrderId, option => option.Ignore())
                .ForMember(x => x.subscribedBundleId, option => option.Ignore());

            CreateMap<ResellerApiBellaService.AvailableBundle, V1.Bundle>()
                .ForMember(x => x.PropositionDiscount, options => options.Ignore());
            CreateMap<ResellerApiBellaService.AdditionalInfo, V1.AdditionalInfo>();
            CreateMap<ResellerApiBellaService.CrmDiscountDto, V1.PropositionDiscount>();
        }
    }
}
