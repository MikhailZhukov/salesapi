﻿using AutoMapper;
using V1 = SalesApi.Models.V1;

namespace SalesApi.Mappings
{
    internal class PropositionProfile : Profile
    {
        public PropositionProfile()
        {
            MapAvailablePropositions();
        }

        private void MapAvailablePropositions()
        {
            CreateMap<ResellerApiBellaService.AvailableProposition, V1.ResellerApiAvailableProposition>()
                .ForMember(x => x.Duration, option => option.Ignore());
            CreateMap<ResellerApiBellaService.GetAvailablePropositionsResponse, V1.ResellerApiAvailablePropositionsResponse>()
            .ForMember(x => x.PossibleExtensions, option => option.Ignore());
        }
    }
}
