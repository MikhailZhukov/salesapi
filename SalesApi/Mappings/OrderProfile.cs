﻿using AutoMapper;
using V1 = SalesApi.Models.V1;

namespace SalesApi.Mappings
{
    internal class OrderProfile : Profile
    {
        public OrderProfile()
        {
            MapOrders();
        }

        private void MapOrders()
        {
            CreateMap<V1.CreateOrder, ResellerApiBellaService.CrmPortalCreateOrderRequest>()
                .ForMember(x => x.additionalInformation, option => option.Ignore())
                .ForMember(x => x.endOn, option => option.Ignore())
                .ForMember(x => x.isOrderFromMyNle, option => option.Ignore())
                .ForMember(x => x.serviceCategoryId, option => option.Ignore())
                .ForMember(
                    x => x.subscribeBundleWithDiscountRequests,
                    opt => opt.MapFrom(y => y.SubscribeBundleWithDiscounts))
                .ForMember(x => x.startOn, option => option.Ignore())
                .ForMember(x => x.tokenId, option => option.Ignore())
                .ForMember(x => x.prolongateCurrentBundles, option => option.Ignore())
                .ForMember(x => x.type, option => option.Ignore())
                .ForMember(x => x.isEnergieCustomer, option => option.Ignore());
            CreateMap<V1.CreateTriplePlayOrder, ResellerApiBellaService.CrmPortalCreateTriplePlayOrderRequest>()
                .ForMember(x => x.portingNumberGiven, option => option.Ignore())
                .ForMember(x => x.salesNumber, option => option.Ignore())
                .ForMember(x => x.salesType, option => option.Ignore())
                .ForMember(x => x.additionalInformation, option => option.Ignore())
                .ForMember(x => x.endOn, option => option.Ignore())
                .ForMember(x => x.isOrderFromMyNle, option => option.Ignore())
                .ForMember(x => x.serviceCategoryId, option => option.Ignore())
                .ForMember(x => x.startOn, option => option.Ignore())
                .ForMember(x => x.salesPartnerOrderId, option => option.Ignore())
                .ForMember(x => x.isUrgent, option => option.Ignore())
                .ForMember(x => x.voipGiven, option => option.Ignore())
                .IncludeBase<V1.CreateOrder, ResellerApiBellaService.CrmPortalCreateOrderRequest>();
            CreateMap<V1.CreateVoipSettings, ResellerApiBellaService.CreateVoipSettingsRequest>();
            CreateMap<V1.PhoneNumberInformation, ResellerApiBellaService.PhoneNumberInformationDto>();
            CreateMap<V1.ContactInfo, ResellerApiBellaService.ContactInfoDto>()
                .ForMember(x => x.phoneInfoList, option => option.MapFrom(y => y.PhoneNumberInformation));
            CreateMap<V1.Address, ResellerApiBellaService.CreateAddressRequest>()
                .ForMember(x => x.apartment, option => option.Ignore());
            CreateMap<V1.CreateOrderCustomerInfo, ResellerApiBellaService.CreateOrderCustomerInfoRequest>()
                .ForMember(x => x.type, option => option.Ignore());
            CreateMap<V1.CreateMandate, ResellerApiBellaService.CreateMandateRequest>()
                .ForMember(x => x.id, option => option.Ignore())
                .ForMember(x => x.startedOn, option => option.MapFrom(y => y.StartOn));
        }
    }
}
