﻿using Microsoft.Extensions.Configuration;

namespace SalesApi.Helpers
{
    internal static class ConfigurationExtentions
    {
        public static string GetVoipTypeOfServiceId(this IConfiguration config) => config.GetValue<string>("VoipTypeOfServiceId");

        public static string GetCobsPostcodeServiceUrl(this IConfiguration config) => config.GetValue<string>("CobsPostcodeServiceUrl");
    }
}
