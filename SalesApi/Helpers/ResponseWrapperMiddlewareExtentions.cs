﻿using Microsoft.AspNetCore.Builder;
using SalesApi.Middleware;

namespace SalesApi.Helpers
{
    internal static class ResponseWrapperMiddlewareExtensions
    {
        public static IApplicationBuilder UseDomainFaultResponseWrapper(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<ResponseWrapperMiddleware>();
        }
    }
}
