﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ResellerApiBellaService;
using SalesApi.Services;
using SalesApi.Settings;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace SalesApi.Helpers
{
    internal static class ServiceExtentions
    {
        public static void ConfigureApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton<ResellerApiServiceClient>();

            services.AddTransient<ICobsPostCodeService, CobsPostCodeService>();

            services.AddTransient(provider => config);
        }

        public static void ConfigureVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(config =>
            {
                var defaultVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                config.DefaultApiVersion = defaultVersion;
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.Conventions.Controller<Controllers.V1.FulfillmentController>().HasApiVersion(defaultVersion);
                config.Conventions.Controller<Controllers.V2.ExampleV2Controller>().HasApiVersion(new Microsoft.AspNetCore.Mvc.ApiVersion(2, 0));
            });

            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(options =>
            {
                options.OperationFilter<SwaggerDefaultValues>();
            });
        }
    }
}
