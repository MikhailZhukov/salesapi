﻿using AutoMapper;
using SalesApi.Mappings;

namespace SalesApi.Helpers
{
    internal static class MapperExtentions
    {
        public static void RegisterMapperProfiles(this IMapperConfigurationExpression expression)
        {
            expression.AddProfile<PropositionProfile>();
            expression.AddProfile<OrderProfile>();
            expression.AddProfile<BundleProfile>();
        }
    }
}
