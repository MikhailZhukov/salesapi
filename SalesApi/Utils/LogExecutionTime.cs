﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NLog;

namespace SalesApi.Utils
{
    internal class LogExecutionTime
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static void Execute(Action action, string methodName, bool fullLog = false)
        {
            var actionId = Guid.NewGuid();
            var sw = new Stopwatch();

            sw.Start();
            if (fullLog)
            {
                _logger.Info(StartExecutingMsg(methodName, actionId));

                action.Invoke();

                sw.Stop();
                _logger.Info(FinishedExecutingMsg(methodName, sw.Elapsed, actionId));
            }
            else
            {
                action.Invoke();

                sw.Stop();
                _logger.Info($"{DateTime.Now.TimeOfDay} Executed: \"{methodName}\", time elapsed: {sw.Elapsed}");
            }
        }

        public static async Task ExecuteAsync(Func<Task> action, string methodName)
        {
            var actionId = Guid.NewGuid();
            var sw = new Stopwatch();

            sw.Start();
            _logger.Info(StartExecutingMsg(methodName, actionId));

            await action.Invoke();

            sw.Stop();
            _logger.Info(FinishedExecutingMsg(methodName, sw.Elapsed, actionId));
        }

        public static T Execute<T>(Func<T> action, string methodName, bool fullLog = false)
        {
            var actionId = Guid.NewGuid();
            var sw = new Stopwatch();

            sw.Start();

            if (fullLog)
            {
                _logger.Info(StartExecutingMsg(methodName, actionId));
            }

            var result = action.Invoke();

            sw.Stop();

            if (fullLog)
            {
                _logger.Info(FinishedExecutingMsg(methodName, sw.Elapsed, actionId));
            }
            else
            {
                _logger.Info($"{DateTime.Now.TimeOfDay} Executed: \"{methodName}\", time elapsed: {sw.Elapsed}");
            }

            return result;
        }

        public static async Task<T> ExecuteAsync<T>(Func<Task<T>> action, string methodName)
        {
            var actionId = Guid.NewGuid();
            var sw = new Stopwatch();

            sw.Start();
            _logger.Info(StartExecutingMsg(methodName, actionId));

            var result = await action.Invoke();

            sw.Stop();
            _logger.Info(FinishedExecutingMsg(methodName, sw.Elapsed, actionId));

            return result;
        }

        private static string StartExecutingMsg(string methodName, Guid actionId)
        {
            return $"{DateTime.Now.TimeOfDay} Start Executing \"{methodName}\", actionId: {actionId}";
        }

        private static string FinishedExecutingMsg(string methodName, TimeSpan elapsed, Guid actionId)
        {
            return $"{DateTime.Now.TimeOfDay} Finished Executing \"{methodName}\", time elapsed: {elapsed}, actionId: {actionId}";
        }
    }
}
