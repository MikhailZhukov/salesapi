using System;
using System.Reflection;
using System.Text.Json.Serialization;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Web;
using SalesApi.Helpers;

namespace SalesApi
{
    public class Startup
    {
        private readonly ILogger _logger;

        // In ASP.NET Core 3.0 and later, it is no longer possible to inject ILogger in Startup.cs and Program.cs. See
        // https://docs.microsoft.com/en-us/aspnet/core/migration/22-to-30?view=aspnetcore-3.0&tabs=visual-studio#hostbuilder-replaces-webhostbuilder
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _logger = NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            services.AddControllers();

            try
            {
                services.ConfigureApplicationServices(Configuration);
                services.ConfigureVersioning();
                services.ConfigureSwagger();
                services.AddAutoMapper(
                    configExpression =>
                {
                    configExpression.RegisterMapperProfiles();
                },
                Array.Empty<Assembly>(),
                ServiceLifetime.Scoped);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error durring configuring services");
                throw;
            }

            _logger.Info("Api services configured successfully");
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            try
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseHsts();
                }

                app.UseHttpsRedirection();
                app.UseRouting();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }

                    c.DisplayRequestDuration();
                });

                app.UseDomainFaultResponseWrapper();
                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error durinf configuring");
                throw;
            }

            _logger.Info("Api configured successfully");
        }
    }
}
